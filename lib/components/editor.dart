import 'package:flutter/material.dart';

class Editor extends StatelessWidget {
  final TextEditingController? controlador;
  final String? nome;
  final String? dica;
  final IconData? icone;

  const Editor({Key? key, this.nome, this.dica, this.controlador, this.icone})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextField(
        controller: controlador,
        style: const TextStyle(fontSize: 20),
        decoration: InputDecoration(
          icon: icone != null ? Icon(icone) : null,
          labelText: nome,
          hintText: dica,
        ),
        keyboardType: TextInputType.number,
      ),
    );
  }
}
