import 'package:bytebank/components/editor.dart';
import 'package:bytebank/models/transferencia.dart';
import 'package:flutter/material.dart';

class FormularioTransferencia extends StatefulWidget {
  const FormularioTransferencia({Key? key}) : super(key: key);

  @override
  State<FormularioTransferencia> createState() =>
      _FormularioTransferenciaState();
}

class _FormularioTransferenciaState extends State<FormularioTransferencia> {
  final TextEditingController _numeroCampo = TextEditingController();

  final TextEditingController _valorCampo = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Formulario de Transferência'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Editor(
              nome: 'Numero da Conta',
              dica: '00000',
              controlador: _numeroCampo,
            ),
            Editor(
              nome: 'Valor',
              dica: '0,0',
              controlador: _valorCampo,
              icone: Icons.monetization_on,
            ),
            ElevatedButton(
              child: const Text('Confirmar'),
              onPressed: () {
                _criarTransferencia(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  void _criarTransferencia(BuildContext context) {
    final int? numero = int.tryParse(_numeroCampo.text);
    final double? valor = double.tryParse(_valorCampo.text);
    if (numero != null && valor != null) {
      final transferencia = Transferencia(valor, numero);
      Navigator.pop(context, transferencia);
    }
  }
}
