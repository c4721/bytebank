import 'package:flutter/material.dart';

import 'pages/transferencia/lista.dart';

void main() => runApp(const BytebankApp());

class BytebankApp extends StatelessWidget {
  const BytebankApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.purple[800],
        primarySwatch: Colors.purple,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.purple[500]),
          ),
        ),
        buttonTheme: ButtonThemeData(buttonColor: Colors.purple[500]),
      ),
      // ignore: prefer_const_constructors
      home: ListaTransferencias(),
    );
  }
}
